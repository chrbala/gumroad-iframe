import "./App.css";
import { useState } from "react";

const DetailPage = ({ title, onCheckout }) => (
  <>
    <h1>{title}</h1>

    <div className="row">
      <p className="col-6">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fringilla
        lacus sit amet augue maximus, quis bibendum tortor ultrices. Lorem ipsum
        dolor sit amet, consectetur adipiscing elit. Nam a ultricies lectus.
        Nullam facilisis, magna eu euismod lacinia, velit velit malesuada sem,
        ut vehicula justo neque at mauris. In et augue vel nisl tincidunt
        lobortis. Curabitur id magna eu nibh efficitur maximus. Orci varius
        natoque penatibus et magnis dis parturient montes, nascetur ridiculus
        mus. Nunc lectus mi, ornare vitae ornare a, rhoncus dapibus arcu. Nulla
        scelerisque imperdiet turpis vitae eleifend. Donec eget risus ipsum.
        Vestibulum tincidunt finibus efficitur.
      </p>

      <div className="col-4">
        <div className="row m-2">
          <button className="btn btn-primary" onClick={onCheckout}>
            Buy now
          </button>
        </div>
        <div className="row m-2">
          <dl className="border border-secondary rounded d-inline-block">
            <div className="row m-1">
              <dt className="col">Size</dt>
              <dd className="col text-nowrap">400 KB</dd>
            </div>
            <hr style={{ margin: 0 }}></hr>
            <div className="row m-1">
              <dt className="col">Length</dt>
              <dd className="col text-nowrap">71 pages</dd>
            </div>
          </dl>
        </div>
      </div>
    </div>
  </>
);

const Checkout = () => (
  <div>This is the checkout page. Please buy my book!</div>
);

const pages = {
  detail: "DETAIL",
  checkout: "CHECKOUT",
};

function App() {
  const isEmbedded = !!window.parent;
  const messageContainer = (value) =>
    window.parent && window.parent.postMessage(JSON.stringify(value), "*");

  const data = new URLSearchParams(window.location.search);
  const title = data.get("title");

  const [page, setPage] = useState(pages.detail);

  return (
    <div className="d-inline-block" style={{ width: "100%" }}>
      <button
        className="btn border rounded-circle btn-secondary close"
        style={{ float: "right" }}
        onClick={() => messageContainer({ action: "close" })}
      >
        X
      </button>
      {page === pages.detail ? (
        <DetailPage title={title} onCheckout={() => setPage(pages.checkout)} />
      ) : page === pages.checkout ? (
        <Checkout />
      ) : null}
    </div>
  );
}

export default App;
